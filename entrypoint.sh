#!/bin/sh
# default entrypoint to docker

# the following command will require all scripts to run
# successfully else it will return an error before continuing.
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# recommended way to start nginx in docker, to tell nginx to run 
# in foreground rather than default background daemon. Which will not
# recommended in docker.
nginx -g 'daemon off;'